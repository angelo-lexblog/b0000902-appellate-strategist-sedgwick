# Blog Name

## Plugins
*	Contact Form 7
*	Co-Authors Plus
*	My Link Order
*	Disqus
*	Feedburner Email Widget
*	Widget Classes
*	WPtouch


## Header Widgets

### Logo (text widget)
*	Content: <h2><a href="#">Published by Author/Firm</a></h2>
*	Classes: logo

### Search
*	Classes: search-box


## Sidebar Widgets

### About (text widget)
*	Title: About This Blog
*	Classes: about

### My Link Order (stay connected)
*	Show Names
*	Classes: stay-connected links

### Feedburner 
*	Title: Subscribe by email
*	Button text: Submit
*	Classes: feedburner

### Categories
*	Title: Topics
*	Classes: links

### Archives
*	Title: Archives
*	Classes: links

### LexBlog Network
*	Title: LexBlog Network
*	Classes: links


## Footer Contact Widgets

### Contact info (text widget) [Same for contact page sidebar]
*	Classes: contact-info
#### Content
<div class="vcard">
<div class="org">Firm Name</div>
<div class="adr">
<div class="street-address">Address 1, Address 2</div>
<span class="locality">City</span>, <span class="region">State</span> <span class="postal-code">Zip</span>
</div>
Phone: <span class="tel">999.999.9999</span><br />
Fax: <span class="fax">999.999.9999</span>
</div>

### My Link Order (stay connected)
*	Show Names
*	Classes: stay-connected


## Footer Extras Widgets

### About (text widget)
*	Title: About Author/Firm
*	Classes: about

### Links
*	Title: Links
*	Classes: links

### Recent Posts
*	Title: Recent Updates
*	Classes: links recent