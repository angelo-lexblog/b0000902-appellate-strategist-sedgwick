<?php
/**
 * The loop that displays posts.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */

// If there are no posts to display, such as an empty archive page
lxb_base_if_no_posts_found();


while ( have_posts() ){
	the_post();

 
//Display all posts
	echo'
	<article '.lxb_base_get_post_class().'>
 		<header class="post-header">
			<h1 class="post-title">
				<a href="'.esc_url(get_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a>
			</h1>'.
			lxb_base_get_post_byline().lxb_base_get_post_categories().'		
		</header>
		<section class="post-content">';
			
			echo lxb_base_get_featured_image('lxb_base_quarter');
			
			the_content( __( 'Continue Reading', 'starkers' ) );
		
	echo'	
		</section>
  
		<footer class="post-footer">';
			echo lxb_base_comments_link();
			if(function_exists('lxb_nascar')){
				echo lxb_nascar();
			}
	echo'
			<div class="post-tools">
			</div>'.
			lxb_base_tags_list().'				
		</footer>
	</article>';

 
} // End the loop. Whew. 

echo lxb_base_if_more_pages();

?>