<?php
/**
 * The loop that displays single posts.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */


if ( function_exists('yoast_breadcrumb')  ) { // display breadcrumbs on single posts only
	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
}

// If there are no posts to display, such as an empty archive page
lxb_base_if_no_posts_found();

while ( have_posts() ) {

	the_post();

	// Display all posts

       echo'
		<article '. lxb_base_get_post_class().'>
        
			<header class="post-header">
				<h1 class="post-title">'.
					get_the_title().'
				</h1>'.
				lxb_base_get_post_byline().lxb_base_get_post_categories().'
			</header>

			<section class="post-content">
   	';
   	
 				echo lxb_base_get_featured_image('lxb_base_half');
   				   	
				the_content();
	echo'
			</section>
	  
<footer class="post-footer">
	';		
	echo lxb_base_print_screen();
	if(function_exists('lxb_nascar')){
				echo lxb_nascar();
	}
	
	echo
				lxb_base_tags_list().'
	
			</footer>
		</article>
';
 
} // End the loop. Whew. ?>